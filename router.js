const express = require("express");
const router = express.Router();

const router_checkout = require("./modules/checkout/checkout.router");

router.use("/ads", router_checkout);

module.exports = router;
