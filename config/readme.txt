
default variables should be in config file

{
    "db":
        {
            "mongo": {
                "host": "host",
                "port": 27017,
                "dbname": "name of db",
                "issecure":false
            }
        },
    "app": {
        "port": 6002
    }, 
    "log":{
        "folderpath":"./log/"
    }
   
}