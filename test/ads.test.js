const rules = require("./rules");
const checkout = require("../modules/checkout/logics/checkout");

/* eslint-disable */
test("Default scenario", async () => {
	const ch = new checkout();
	ch.new(rules, "default id");
	ch.add("classic");
	ch.add("standout");
	ch.add("premium");
	const total = await ch.total();
	expect(total.price).toBe(987.97);
});

test("Default scenario using summary", async () => {
	const ch = new checkout();
	ch.new(rules, "default id");
	ch.addSummary({ classic: 1, standout: 1, premium: 1 });
	const total = await ch.total();
	expect(total.price).toBe(987.97);
});

test("Unilever scenario", async () => {
	const ch = new checkout();
	ch.new(rules, "Unilever");
	ch.add("classic");
	ch.add("classic");
	ch.add("classic");
	ch.add("premium");
	const total = await ch.total();
	expect(total.price).toBe(934.97);
});

test("Unilever scenario using summary", async () => {
	const ch = new checkout();
	ch.new(rules, "Unilever");
	ch.addSummary({ classic: 3, premium: 1 });
	ch.add("classic");
	ch.add("classic");
	ch.add("classic");
	ch.add("premium");
	const total = await ch.total();
	expect(total.price).toBe(934.97);
});

test("Apple scenario", async () => {
	const ch = new checkout();
	ch.new(rules, "Apple");
	ch.add("standout");
	ch.add("standout");
	ch.add("standout");
	ch.add("premium");
	const total = await ch.total();
	expect(total.price).toBe(1294.96);
});

test("Apple scenario using summary", async () => {
	const ch = new checkout();
	ch.new(rules, "Apple");
	ch.addSummary({ standout: 3, premium: 1 });
	const total = await ch.total();
	expect(total.price).toBe(1294.96);
});

test("Nike scenario", async () => {
	const ch = new checkout();
	ch.new(rules, "Nike");
	ch.add("premium");
	ch.add("premium");
	ch.add("premium");
	ch.add("premium");
	const total = await ch.total();
	expect(total.price).toBe(1519.96);
});

test("Nike scenario using summary", async () => {
	const ch = new checkout();
	ch.new(rules, "Nike");
	ch.addSummary({ premium: 4 });

	const total = await ch.total();
	expect(total.price).toBe(1519.96);
});
