-Ads is a library to checkout scenarios requested.
-All example scenarios are tested by automate unit test in /test folder
-Checkout is expecting pricerules and clientname.
-You can find sample rules as explained in the question in /test/rules.js
-Please follow below steps to see the result.

	1- make sure nodejs installed 
	2- open shell or cmd
	3- go to the /ads folder
	4- run "npm i"
	5- run "npm test"

Thanks.
