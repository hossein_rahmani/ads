const express = require("express");
const app = express();

require("express-async-errors");
const bodyParser = require("body-parser");
const debug = require("debug")("app:debug");
const config = require("config");
const errors = require("./helpers/error");
const cors = require("cors");
//const mongo = require("./helpers/mongoos-init")();
const { Joi } = require("celebrate");
Joi.objectId = require("joi-objectid")(Joi);

const router = require("./router");

const swagger = require("./helpers/swagger-doc");
const expressSwagger = require("express-swagger-generator")(app);
const env = process.env.NODE_ENV || "development";
debug(`current environment is ${env}`);

//mongo.init();

app.use(cors());
app.use(
	bodyParser.urlencoded({
		extended: false
	})
);
app.use(bodyParser.json());

app.use("/", router);
app.use(errors);

var ap = app.listen(config.get("app.port"), function() {
	debug(`Ads API listening on port ${config.get("app.port")} !`);
});
if (env === "development") expressSwagger(swagger);

module.exports = ap;
