const mongoose = require('mongoose');
const debug = require('debug')('app:debug');
const config = require('config');

const mongooseInit = function () {

	function init() {
		let connectionString;
		if (config.get('db.mongo.issecure')) {
			connectionString = `mongodb://${config.get('db.mongo.username')}:
            ${config.get('db.mongo.password')}@${config.get('db.mongo.host')}:
            ${config.get('db.mongo.port')}/${config.get('db.mongo.dbname')}`;
		} else {
			connectionString = `mongodb://${config.get('db.mongo.host')}/${config.get('db.mongo.dbname')}`;
		}

		mongoose.connect(connectionString, { useNewUrlParser: true }).then(() => {
			debug('Mongo initialized');

		},
			err => {
				debug('Fail to initialize Mongo');
				debug(JSON.stringify(err))
				throw err;
			});
	}

	function dispose() {
		debug('inside mongoDB dispose');
		mongoose.connection.close();
	}

	function getStatus() {
		debug(mongoose.connection.readyState);
	}

	return {
		init: init,
		dispose: dispose,
		getStatus: getStatus
	};
};

module.exports = mongooseInit;
