"use strict";
module.exports = {
	swaggerDefinition: {
		info: {
			description: "Swagger Doc for NMS API",
			title: "NMS API",
			version: "1.0.0"
		},
		host: "localhost:3001",
		basePath: "/",
		produces: ["application/json"],
		schemes: ["http"]
	},
	basedir: __dirname,
	files: ["../modules/**/*.router*.js"]
};
