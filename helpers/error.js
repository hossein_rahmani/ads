const { isCelebrate } = require('celebrate');
const debug = require('debug')('app:debug');
const logger = require('./logger')();

module.exports = function (err, req, res, next) {

    if (isCelebrate(err)) {
        logger.warn(err);
        res.status(400).send(err.message);
    }
    else {
        logger.error(err);
        res.status(500).send(err.message);
    }
};