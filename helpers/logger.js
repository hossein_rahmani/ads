const winston = require('winston');
require('winston-daily-rotate-file');
const config = require('config');
const logfolder = config.get('log.folderpath');
const debug = require('debug')('app:debug');

function Logger () {
    var ErrorFileTransport = new (winston.transports.DailyRotateFile)({
        filename: logfolder + 'error-%DATE%.log',
        datePattern: 'YYYY-MM-DD-HH',
        zippedArchive: true,
        maxSize: '20m',
        maxFiles: '14d',
        level: 'error'
    });

    var WarningFileTransport = new (winston.transports.DailyRotateFile)({
        filename: logfolder + 'warning-%DATE%.log',
        datePattern: 'YYYY-MM-DD-HH',
        zippedArchive: true,
        maxSize: '20m',
        maxFiles: '14d',
        level: 'warn'
    });
    const logger = winston.createLogger({
        transports: [ErrorFileTransport,WarningFileTransport]
    });

    const error = function (err) {
        debug(err);
        logger.error(err.message,{time:new Date().toLocaleTimeString(),stack:err.stack});
    };

    const warn = function (err) {
        debug(err);
        logger.warn(err.message,{time:new Date().toLocaleTimeString(),stack:err.stack});
    };

    return {
        error,
        warn
    };
}

module.exports = Logger;
