"use strict";
const debug = require("debug")("app:debug");
const checkoutLogic = require("./logics/checkout");
const rules = require("./logics/rules");

const checkoutController = () => {
	const checkout = async (req, res) => {
		const client = req.body.customer;
		const products = req.body.products;
		const checkoutLogicobj = new checkoutLogic();

		checkoutLogicobj.new(rules, client);
		checkoutLogicobj.addSummary(products);
		debug(products);
		const total = await checkoutLogicobj.total();
		debug(total);
		return res.send(total);
	};

	return {
		checkout
	};
};

module.exports = checkoutController;
