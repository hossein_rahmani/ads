const { Joi } = require("celebrate");

const alarmValidationModel = {
	checkout: {
		body: {
			customer: Joi.string().required(),
			products: Joi.object({
				classic: Joi.number()
					.min(0)
					.max(10),
				standout: Joi.number()
					.min(0)
					.max(10),
				premium: Joi.number()
					.min(0)
					.max(10)
			})
		}
	}
};

module.exports = alarmValidationModel;
