let Engine = require("json-rules-engine").Engine;


const ruleEngine = () => {
	const runner = async (Conditions, adsData) => {
		let result;
		let results=[];
		for (const con of Conditions) {
			//console.log(JSON.stringify(con,null,4));
			const engine = new Engine();
			const rule = con;
			engine.addRule(rule);
			try {
				result = await engine.run(adsData);
				if (result.length > 0) {
					results.push(result[0]);
					//break;
				}
			} catch (error) {
				console.log("--->" + error);
				throw new Error(error);
			}
		}
		return results;
	};

	return {
		run: runner
	};
};

module.exports = ruleEngine;
