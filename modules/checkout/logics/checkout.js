const engine = require("./ruleEngine")();
const _ = require("lodash");

module.exports = function() {
	this.items = [];
	this.itemsSummary = {
		customer: "",
		classic: 0,
		standout: 0,
		premium: 0,
		price: 0
	};
	this.rules = [];
	this.new = (rules, customer) => {
		this.rules = rules;
		this.itemsSummary.customer = customer;
	};

	this.add = item => {
		this.items.push(item);
		this.summarize();
	};

	this.addSummary = item => {
		this.itemsSummary.classic = item.classic ? item.classic : 0;
		this.itemsSummary.standout = item.standout ? item.standout : 0;
		this.itemsSummary.premium = item.premium ? item.premium : 0;
	};

	this.calc = (product, Items, priceRules) => {
		const _priceRules = priceRules.filter(
			i => i.params.productType === product
		);
		let _priceRule = "";
		if (_priceRules.length > 0) {
			_priceRule = _priceRules[0];
			let count = Items[product];

			switch (_priceRule.type) {
				case "fix":
					Items.price += count * _priceRule.params.price;
					break;
				case "xfory":
					count -= Math.floor(count / _priceRule.params.x);
					Items.price += count * _priceRule.params.price;
					break;
				default:
					break;
			}
		} else {
			return Items;
		}
		return Items;
	};

	this.summarize = async () => {
		if (this.items.length > 0) {
			(this.itemsSummary.classic = this.items.filter(
				i => i === "classic"
			).length),
				(this.itemsSummary.standout = this.items.filter(
					i => i === "standout"
				).length);
			this.itemsSummary.premium = this.items.filter(
				i => i === "premium"
			).length;
		}
	};

	this.total = async () => {
		let priceRules = await engine.run(this.rules, this.itemsSummary);

		this.itemsSummary = this.calc("classic", this.itemsSummary, priceRules);
		this.itemsSummary = this.calc("standout", this.itemsSummary, priceRules);
		this.itemsSummary = this.calc("premium", this.itemsSummary, priceRules);
		this.itemsSummary = _.omit(this.itemsSummary, "success-events");
		return this.itemsSummary;
	};
};
