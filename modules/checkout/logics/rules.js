module.exports = [{
		"conditions": {
			"all": [{
					"fact": "customer",
					"operator": "equal",
					"value": "Unilever"
				},
				{
					"fact": "classic",
					"operator": "greaterThanInclusive",
					"value": 3
				}
			]
		},
		"event": {
			"type": "xfory",
			"params": {
				"promoTitle": "unilever_promo_3for2",
				"productType": "classic",
				"x": 3,
				"y": 2,
				"price": 269.99
			}
		}
	},
	{
		"conditions": {
			"all": [{
					"fact": "customer",
					"operator": "equal",
					"value": "Apple"
				},
				{
					"fact": "standout",
					"operator": "greaterThanInclusive",
					"value": 1
				}
			]
		},
		"event": {
			"type": "fix",
			"params": {
				"promoTitle": "apple_promo_standout",
				"productType": "standout",
				"price": 299.99
			}
		}
	},
	{
		"conditions": {
			"all": [{
					"fact": "customer",
					"operator": "equal",
					"value": "Nike"
				},
				{
					"fact": "premium",
					"operator": "greaterThanInclusive",
					"value": 4
				}
			]
		},
		"event": {
			"type": "fix",
			"params": {
				"promoTitle": "nike_promo_premium",
				"productType": "premium",
				"price": 379.99
			}
		}
	},
	{
		"conditions": {
			"all": [{
					"fact": "customer",
					"operator": "equal",
					"value": "Ford"
				},
				{
					"fact": "classic",
					"operator": "greaterThanInclusive",
					"value": 5
				}
			]
		},
		"event": {
			"type": "xfory",
			"params": {
				"promoTitle": "ford_promo_5for4",
				"productType": "classic",
				"x": 5,
				"y": 4,
				"price": 269.99
			}
		}
	},
	{
		"conditions": {
			"all": [{
					"fact": "customer",
					"operator": "equal",
					"value": "Ford"
				},
				{
					"fact": "standout",
					"operator": "greaterThanInclusive",
					"value": 1
				}
			]
		},
		"event": {
			"type": "fix",
			"params": {
				"promoTitle": "ford_promo_standout",
				"productType": "standout",
				"price": 309.99
			}
		}
	},
	{
		"conditions": {
			"all": [{
					"fact": "customer",
					"operator": "equal",
					"value": "Ford"
				},
				{
					"fact": "premium",
					"operator": "greaterThanInclusive",
					"value": 3
				}
			]
		},
		"event": {
			"type": "fix",
			"params": {
				"promoTitle": "ford_promo_premium",
				"productType": "premium",
				"price": 389.99
			}
		}
	},//----------no promo----------------------------------------------
	{
		"conditions": {
			"all": [
				{
					"fact": "classic",
					"operator": "greaterThanInclusive",
					"value": 1
				}
			]
		},
		"event": {
			"type": "fix",
			"params": {
				"promoTitle": "classic",
				"productType": "classic",
				"price": 269.99
			}
		}
	},
	{
		"conditions": {
			"all": [
				{
					"fact": "standout",
					"operator": "greaterThanInclusive",
					"value": 1
				}
			]
		},
		"event": {
			"type": "fix",
			"params": {
				"promoTitle": "standout",
				"productType": "standout",
				"price": 322.99
			}
		}
	},
	{
		"conditions": {
			"all": [
				{
					"fact": "premium",
					"operator": "greaterThanInclusive",
					"value": 1
				}
			]
		},
		"event": {
			"type": "fix",
			"params": {
				"promoTitle": "premium",
				"productType": "premium",
				"price": 394.99
			}
		}
	}
]
