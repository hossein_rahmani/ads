const checkoutController = require("./checkout.controller")();

const vmodel = require("./models/checkout.validation.model");
const { celebrate } = require("celebrate");

const router = require("express").Router();

const validate = vModel => {
	let vmodelObject = vModel;
	return celebrate(vmodelObject);
};

/**
 * This API allows UI application to apply conditional discounts and return the total price.
 * @group Ads
 * @route POST /ads/checkout
 * @param { checkout.model } checkout.body.required
 * @returns { basket_response.model } 200 - basket summary
 * @produces application/json
 * @consumes application/json
 */
router.post(
	"/checkout",
	validate(vmodel.checkout),
	checkoutController.checkout
);

module.exports = router;
